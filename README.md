# README #

### What is this repository for? ###

* Macro generator
* Version 1.0


### How do I get set up? ###

* Have AutoHotKey installed on your machine
* run node app.js
* run the generated macros.ahk file on your machine and you will now have macros assigned to numpad keys.

Written by Oliver Church