const readline = require('readline');
var fs = require('fs');


class MacroBuddy{
    constructor(){}
    async process(){
        const count = await this.askQuestion(`How many macros do you need? \n`)
        console.log(`${count} macros will be created.\n`)
        let i = 0
        let macros = Array(count)
        while (i < count){
            const ans = await this.askQuestion(`Type the line you wish to macro\n`)
            console.log(`Macro for ${ans} created`)
            macros[i] = ans
            i++
        }
        this.createMacroFile(macros);
    }
    async askQuestion (query) {
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout,
        });

        return new Promise(resolve => rl.question(query, ans => {
            rl.close();
            resolve(ans);
        }))
    }
    async createMacroFile(macros){
        let content = ''
        macros.forEach((value,index) => {
            content +=
`Numpad${index+1}::
SendRaw ${value}
return\n`
        })


        fs.writeFile('macro.ahk', content, function (err) {
            if (err) throw err;
            console.log('Saved!');
        });
    }
}


const test = new MacroBuddy()
test.process()

